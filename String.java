package com.hillel;
import java.util.Scanner;

public class String {
    public static void main(java.lang.String[] args) {

        System.out.println(compareStrings(getString(), getString()));
    }

    public static java.lang.String getString() {
        System.out.println("Enter your string:");
        Scanner scanner = new Scanner(System.in);
        java.lang.String str = scanner.nextLine();
        return str;
    }

    public static int compareStrings(java.lang.String s1, java.lang.String s2) {
        StringBuilder stringBuilder1 = new StringBuilder(s1);
        StringBuilder stringBuilder2 = new StringBuilder(s2);
        if (checkForEqual(count(stringBuilder1), count(stringBuilder2), stringBuilder1, stringBuilder2)) {
            return 1;
        }
        else {
            return 0;
        }
    }

    public static boolean checkForEqual(int counter1, int counter2, StringBuilder str, StringBuilder str2) {
        if (counter1 != counter2) {
            return false;
        }
        else if (counter1 == -1) {
            return true;
        }
        else {
            for (int i = 0; i < str.length(); ++i) {
                if (str.charAt(i) != str2.charAt(i)) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    public static int count (StringBuilder str) {
        int counter = -1;
        for (int i = 0; i < str.length(); ++i) {
            if (str.charAt(i) == '#' && counter != -1) {
                counter -= 1;
            }
            else if (str.charAt(i) != '#') {
                str.setCharAt(counter + 1, str.charAt(i));
                counter +=1;
            }
        }
        return counter;
    }
}
